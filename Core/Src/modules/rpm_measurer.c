#include "rpm_measurer.h"
#include "irq.h"
#include "time_src.h"
#include <string.h>

#define TIME_BASE_MS 1000000UL

void rpm_measurer_init(struct speed_measure_t * s,
                       uint32_t encoder_holes,
                       uint32_t time_base,
                       uint32_t not_measured_interval)
{
    memset(s, 0, sizeof(*s));
    s->encoder_holes = encoder_holes;
    s->time_base = time_base;
    s->degrees_at_time = time_base / encoder_holes;
    s->not_measured_interval = not_measured_interval;
}

void rpm_measurer_init_time_ms(struct speed_measure_t * s, uint32_t encoder_holes)
{
  rpm_measurer_init(s, encoder_holes, TIME_BASE_MS, 5000);
}

 // recalculate degrees_at_time into degress (koef = 360) or radians (koef = 2 * pi)
void rpm_measurer_init_degrees_koef(struct speed_measure_t * s, float koef)
{
    s->degrees_at_time *= koef;
}

void rpm_measurer_init_relevance_interval(struct speed_measure_t * s, uint32_t ri)
{
    s->relevance_interval = ri;
}

#define FRM_TIME(tmr) (tmr.ms * 1000 + tmr.rest_us)

void calculate_rpm(struct speed_measure_t * rpm)
{
  wrp_disable_irq();
  struct val_time_t tmr = get_current_time();
  uint32_t dt = FRM_TIME(tmr) - FRM_TIME(rpm->last_measured_time);
  if (dt > rpm->not_measured_interval)
  {
    rpm->speed = rpm->degrees_at_time / dt;
    rpm->last_measured_time = tmr;
  }
  wrp_enable_irq();
}

float current_speed(struct speed_measure_t * rpm)
{
  struct val_time_t tmr = get_current_time();
  uint32_t dt = FRM_TIME(tmr) - FRM_TIME(rpm->last_measured_time);
  if (dt >= 0 && dt <= rpm->relevance_interval)
    return rpm->speed;
  return 0;
}

#undef FRM_TIME