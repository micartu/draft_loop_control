#ifndef __RPM_MEASURER_H__
#define __RPM_MEASURER_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stddef.h>
#include "time_src.h"

struct speed_measure_t
{
	/// measured speed at time <last_measured_time>
	float speed;
	/// in how many holes the circle of encoder is divided
	uint32_t encoder_holes;
	/// time within the speed will be still relevant
	uint32_t relevance_interval;
	/// timestampt of last speed's estimate
	struct val_time_t last_measured_time;
	/// a koefficient to calculate speed in degrees/rad.
	float degrees_at_time;
	uint32_t time_base;
	uint32_t not_measured_interval;
};

/**
 * Initializes measurer unit struct
 * which contains all information needed to calculate the RPM of a wheel
 *
 * @param s structure to be initialized
 * @param encoder_holes how many holes our encoder contains
 * @param time_base upper numerator which represents time elapsed in one time's unit
 * @param not_measured_interval time interval which should be ignored
 */
void rpm_measurer_init(struct speed_measure_t * s,
					   uint32_t encoder_holes,
					   uint32_t time_base,
					   uint32_t not_measured_interval);

/**
 * Initializes measurer unit struct, shortened version
 *
 * @param s structure to be initialized
 * @param encoder_holes how many holes our encoder contains
 */
void rpm_measurer_init_time_ms(struct speed_measure_t * s, uint32_t encoder_holes);

/**
 * Multiples degrees_at_time at the given constant
 *
 * @param s structure to be initialized
 * @param koef a constant that must be multiplied with degrees_at_time in
 * order to scale it to degrees or radians (if needed)
 */
void rpm_measurer_init_degrees_koef(struct speed_measure_t * s, float koef);

/**
 * @brief setups relevance interval in the given bundle
 * 
 * @param s bundle where relevance interval should be installed
 * @param ri relevance interval itself
 */
void rpm_measurer_init_relevance_interval(struct speed_measure_t * s, uint32_t ri);

/**
 * Calculates RPM based on previous values
 * Must be called in IRQ from EXT Output's PIN
 *
 * @param rpm structure which contains all needed data for measuring RPM
 */
void calculate_rpm(struct speed_measure_t * rpm);

/**
 * @brief returns the current estimated speed of the wheel
 * 
 * @param rpm all data needed to estimate the spee
 * @return currenly estimated speed
 */
float current_speed(struct speed_measure_t * rpm);

#ifdef __cplusplus
}
#endif

#endif /* __RPM_MEASURER_H__ */