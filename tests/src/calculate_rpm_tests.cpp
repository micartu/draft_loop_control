#include "CppUTest/TestHarness.h"
#include "rpm_measurer.h"

#define kHoles 20

// needed headers + and implementations which control fake's behavior:
extern "C"
{
    void _set_sys_time(uint32_t time);
}

TEST_GROUP(CalculateRPMTestGroup)
{
    struct speed_measure_t rpm;

    void setup()
    {
        rpm_measurer_init_time_ms(&rpm, kHoles);
        rpm_measurer_init_degrees_koef(&rpm, 360);
    }

    void teardown()
    {
    }
};

TEST(CalculateRPMTestGroup, InitializationTest)
{
    LONGS_EQUAL(0, rpm.last_measured_time.ms);
    LONGS_EQUAL(kHoles, rpm.encoder_holes);
}

TEST(CalculateRPMTestGroup, CalculateRPMTest)
{
    // given
    uint32_t dT = 10;
    _set_sys_time(0);
    calculate_rpm(&rpm);

    // when
    _set_sys_time(dT);
    calculate_rpm(&rpm);

    // then
    LONGS_EQUAL(dT, rpm.last_measured_time.ms);
    DOUBLES_EQUAL(360 * 5, rpm.speed, 0.1);
}

TEST(CalculateRPMTestGroup, EstimateSpeedWithinRelevanceInterval)
{
    // given
    uint32_t dT = 10;
    _set_sys_time(0);
    rpm_measurer_init_relevance_interval(&rpm, 50);
    calculate_rpm(&rpm);
    _set_sys_time(dT);
    calculate_rpm(&rpm);

    // when
    float speed = current_speed(&rpm);

    // then
    DOUBLES_EQUAL(360 * 5, speed, 0.1);
}

TEST(CalculateRPMTestGroup, EstimateSpeedOutOfRI)
{
    // given
    uint32_t dT = 10;
    _set_sys_time(0);
    rpm_measurer_init_relevance_interval(&rpm, 50);
    calculate_rpm(&rpm);
    _set_sys_time(dT);
    calculate_rpm(&rpm);

    // when
    _set_sys_time(dT + 51);
    float speed = current_speed(&rpm);

    // then
    DOUBLES_EQUAL(0, speed, 0.1);
}